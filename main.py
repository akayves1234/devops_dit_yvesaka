import directory_creator as dc

def main():
    local_directory = "MyProject"
    
    tree = [
        "data",
        "docs",
        "notebooks",
        "reports",
        "src",
        "models",
        ["README.md"],
        ["LICENSE"],
        ["Makefile"],
        ["requirements.txt"],
        ["notebooks","main.ipynb"],
        ["src","utils.py"]
    ]
    
    directory_creator = dc.DirectoryCreator(local_directory)
    directory_creator.setup_tree(tree)
    
    dc.first_commit(local_dir=".", commit_message="First generated commit")
    dc.new_commits(local_dir=".", commit_times=5)
    dc.push_repository(repo_url="git@gitlab.com:akayves1234/devops_dit_yvesaka.git", local_dir=".")
    print("Folder tree created and commits performed successfully!")

if __name__ == "__main__":
    main()