import os
import subprocess

class DirectoryCreator:
    def __init__(self, local_dir):
        self.local_dir = local_dir

    def setup_tree(self, tree):
        for item in tree:
            if isinstance(item, str):  # If it's a folder name
                folder_path = os.path.join(self.local_dir, item)
                os.makedirs(folder_path, exist_ok=True)
            elif isinstance(item, tuple):  # If it's a nested tree
                folder_name, subtree = item
                folder_path = os.path.join(self.local_dir, folder_name)
                os.makedirs(folder_path, exist_ok=True)
                sub_directory_creator = DirectoryCreator(folder_path)
                sub_directory_creator.setup_tree(subtree)
            elif isinstance(item, list):
                if len(item) == 2:
                    folder_name, file = item
                    os.makedirs(os.path.join(self.local_dir, folder_name), exist_ok=True)
                    open(os.path.join(self.local_dir, folder_name, file), "w")
                else:
                    open(os.path.join(self.local_dir, item[0]), "w")
                    

def first_commit(local_dir, commit_message):
    subprocess.run(["git", "init","--initial-branch=main"], cwd=local_dir)
    subprocess.run(["git", "add", "."], cwd=local_dir)
    subprocess.run(["git", "commit", "-m", commit_message], cwd=local_dir)

def new_commits(local_dir, commit_times):
    for i in range(commit_times):
        with open(os.path.join(local_dir, "commit.txt"), "w") as f:
            f.write(f"This is commit {i+1}")
        subprocess.run(["git", "add", "."], cwd=local_dir)
        subprocess.run(["git", "commit", "-m", f"New commit {i+1}"], cwd=local_dir)

def push_repository(repo_url, local_dir):
    subprocess.run(["git", "remote", "add", "origin", repo_url], cwd=local_dir)
    subprocess.run(["git", "push", "-u", "origin", "main"], cwd=local_dir)